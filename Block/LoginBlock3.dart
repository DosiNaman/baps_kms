import 'package:baps_kms/Block/Style.dart';
import 'package:flutter/material.dart';

class LBlock3 extends StatefulWidget {

  @override
  _LBlock3State createState() => _LBlock3State();
}

class _LBlock3State extends State<LBlock3> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Password",
          style: email_pass
        ),
        SizedBox(
          height: 10.0,
        ),
        TextFormField(
          obscureText: true,
          style: TextStyle(fontSize: 24.0),
          decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 10,horizontal: 8.0),
              border: OutlineInputBorder(
                  borderSide:
                  BorderSide(width: 2.0, color: Colors.grey))),
        ),
      ],
    );
  }
}
