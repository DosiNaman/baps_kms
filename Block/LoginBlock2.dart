import 'package:baps_kms/Block/Style.dart';
import 'package:flutter/material.dart';

class LBlock2 extends StatefulWidget {


  @override
  _LBlock2State createState() => _LBlock2State();
}

class _LBlock2State extends State<LBlock2> {

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Email Address",
          style: email_pass,
        ),
        SizedBox(
          height: 10.0,
        ),
        TextFormField(
          keyboardType: TextInputType.emailAddress,
          style: TextStyle(fontSize: 24.0),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 10,horizontal: 8.0),
              border: OutlineInputBorder(
                  borderSide:
                  BorderSide(width: 2.0, color: Colors.grey))),
        ),
      ],
    );
  }
}
