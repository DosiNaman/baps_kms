import 'dart:ffi';

import 'package:baps_kms/Block/Style.dart';
import 'package:baps_kms/screen/DailogBox.dart';
import 'package:baps_kms/screen/datetime.dart';
import 'package:baps_kms/screen/menu.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';


class MealEvent extends StatefulWidget {
  @override
  _MealEventState createState() => _MealEventState();
}

class _MealEventState extends State<MealEvent> {

  final String url = "http://134.209.159.163/baps_kms/api/event";
  List data;

  @override void initState() {
    // super.initState();
    this.getJsonData();
    super.initState();
  }

  Future<String> getJsonData() async {
    var response = await http.get(
      Uri.encodeFull(url),
      //headers: {"Aceept": "application/jason"}
    );
    print(response.body);
    setState(() {
      var convertDataTOJason = json.decode(response.body);

      data = convertDataTOJason['data'];
    });
    return "Sucess";
  }


  @override
  Widget build(BuildContext context) {
    String selected;

    return ListView.builder(
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (context, index) {
          int actual = data[index]['meal']['actual_count'];
          String meali = data[index]['meal']['id'];
          var parseDate = DateTime.parse(data[index]['date']);
          var Formatted = DateFormat.yMMMd().format(parseDate);
          List<String>names = [];
          List id = [];
          String eventid = data[index]['meal']['event_id'];
          for (int i = 0; i < data[index]['meal']['menus'].length; i++) {
            if (data[index]['meal']['menus'][i]['meal_id'] ==
                data[index]['meal']['id']) {
              names.add(data[index]['meal']['menus'][i]['name'].toString());
              id.add(data[index]['meal']['menus'][i]['id']);
            }
          }
          print(names);
          print(id);
          return Container(
            decoration: BoxDecoration(
                border: Border.all(
                    width: 0.5,
                    color: Colors.grey[400]
                )
            ),
            padding: EdgeInsets.only(left: 15.0, top: 12.0, bottom: 12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, top: 3.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(data[index]['event_type'],
                            style: event_type,
                            textAlign: TextAlign.left,
                          ),
                          Text(data[index]['meal']['type'],
                            style: meal_type,
                          ),
                        ],
                      ),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(Formatted,
                            style: date
                        ),
                        PopupMenuButton(
                            tooltip: 'Menu',
                            icon: Icon(Icons.more_vert,
                              color: secondaryText,),

                            onSelected: (value) {
                              if (value == 'Edit') {
                                Navigator.push(
                                    context,MaterialPageRoute(builder: (
                                    context)
                                =>
                                    Timeinput(name: names,
                                      meal_id: meali,
                                      actualcount: actual,
                                      id: id,
                                      event_id: eventid,)
                              )
                              );
                            }
//                              else if (value =='Edit'){
//                                Navigator.push(
//                                    context,
//                                    MaterialPageRoute(
//                                    builder: (context) => MenuList(names,meali)));
//                              }
                            },
                            itemBuilder: (context) =>
                            [
                              PopupMenuItem(
                                value: 'Edit',
                                child: Text('Edit',
                                  style: popmenutext,),
                              ),
//                              PopupMenuItem(
//                                value: 'update_count',
//                                child: Text('Update Count',
//                                style: popmenutext,),
//                              )

                            ]),
                      ],
                    )
                  ],
                ),

                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Expected Count', style: count
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(4.0)),
                              child: Container(
                                color: colorPrimaryLight,
                                width: 50.0,
                                height: 25.0,
                                child: Center(
                                  child: Text(
                                    data[index]['meal']['plan_guest_count'],
                                    style: count,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Text('Start Time', style: count),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(4.0)),
                              child: Container(
                                color: colorPrimaryLight,
                                width: 80.0,
                                height: 25.0,
                                child: Center(
                                  child: Text(data[index]['meal']['start_time'],
                                    style: count,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Actual Count',
                            style: count,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(4.0)),
                              child: Container(
                                color: secondaryLight,
                                width: 50.0,
                                height: 25.0,
                                child: Center(
                                  child: Text('$actual',
                                    style: count,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Text('End Time', style: count,),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(4.0)),
                              child: Container(
                                color: secondaryLight,
                                width: 80.0,
                                height: 25.0,
                                child: Center(
                                  child: Text(data[index]['meal']['end_time'],
                                    style: count,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),


              ],
            ),
          );
        });
  }

}

