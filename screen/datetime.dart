import 'package:baps_kms/Block/Style.dart';
import 'package:baps_kms/screen/menu.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Timeinput extends StatefulWidget {
  List<String>name;
  String event_id;
  String meal_id;
  int actualcount;
  List id;

  Timeinput(
      {this.name, this.meal_id, this.actualcount, this.id, this.event_id});

  @override
  _TimeinputState createState() => _TimeinputState();
}

class _TimeinputState extends State<Timeinput> {
  var _starttime = TextEditingController();
  var _endTime = TextEditingController();
  final format = DateFormat("HH:mm");
  List _dinningArea = [
    "Male",
    "Female",
    "Male&Female",
    "Guest Dinning",
    "BKG Dinning",
    "Other"
  ];
  List _buffetstation = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"];

  List<DropdownMenuItem<String>> _dropbufferiteam;
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentArea = "";
  String buffet_station = "0";
  String station="";

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentArea = _dropDownMenuItems[0].value;
    _dropbufferiteam = getDropDownItems();
    station = _dropbufferiteam[0].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String areas in _dinningArea) {
      items.add(new DropdownMenuItem(value: areas, child: new Text(areas)));
    }
    return items;
  }
  List<DropdownMenuItem<String>> getDropDownItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String areas in _buffetstation) {
      items.add(new DropdownMenuItem(value: areas, child: new Text(areas)));
    }
    return items;
  }
  void changedDropDownItem(String selectedZone) {
    setState(() {
      _currentArea = selectedZone;
    });
  }
  void changedItem(String selectedZone) {
    setState(() {
      station = selectedZone;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select Time'),
        elevation: 2,
        centerTitle: true,
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.only(left: 12.0, right: 12.0, top: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 12,
            ),
            Text('Start Time', style: dailogText2,),
            DateTimeField(
              controller: _starttime,
              format: format,
              onShowPicker: (context, currentValue) async {
                final startime = await showTimePicker(
                  context: context,
                  initialTime: TimeOfDay.fromDateTime(
                      currentValue ?? DateTime.now()),
                );
                return DateTimeField.convert(startime);
              },
            ),
            SizedBox(
              height: 12,
            ),
            Text('End Time', style: dailogText2,),
            DateTimeField(
                decoration: InputDecoration(
                ),
                controller: _endTime,
                format: format,
                onShowPicker: (context, currentValue) async {
                  final endtime = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.fromDateTime(
                        currentValue ?? DateTime.now()),
                  );
                  return DateTimeField.convert(endtime);
                },
            ),
            SizedBox(
              height: 10.0,
            ),
            Text('Dinning Area',
                style: dailogText2),
            SizedBox(
              height: 10,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: Container(
                padding: EdgeInsets.only(left: 5.0),
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    shape: BoxShape.rectangle
                ),
                child: DropdownButton(
                  style: dailogText3,
                  isDense: false,
                  isExpanded: true,
                  value: _currentArea,
                  items: _dropDownMenuItems,
                  onChanged: changedDropDownItem,
                  underline: Container(
                    decoration: BoxDecoration(
                        color: Colors.transparent
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('Buffet Station', style: dailogText2),
            SizedBox(
              height: 10,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: Container(
                padding: EdgeInsets.only(left: 5.0),
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    shape: BoxShape.rectangle
                ),
                child: DropdownButton(
                  style: dailogText3,
                  isDense: false,
                  isExpanded: true,
                  value: station,
                  items: _dropbufferiteam,
                  onChanged: changedItem,
                  underline: Container(
                    decoration: BoxDecoration(
                        color: Colors.transparent
                    ),
                  ),
                ),
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: RaisedButton(
                    onPressed: () async {
                      print(_starttime.text);
                      FormData formdata = FormData.fromMap({
                       'dinning_area':'$_currentArea',
                        'buffet_station':'$station',
                        'actual_start_time':'${_starttime.text}',
                        'actual_end_time':'${_endTime.text}',
                        'event_meal_id':'${widget.meal_id}'

                      });
                      Dio dio = Dio();
                      Response response = await dio.post("http://134.209.159.163/baps/api/meal/setActualTime",
                        data: formdata,
                      );
                      print(response);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => MenuList(
                            names: widget.name,
                            mealid: widget.meal_id,
                            is_avial: widget.id,)));
                    },
                    child: Text('Next', style: dailogText,),
                    color: primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
