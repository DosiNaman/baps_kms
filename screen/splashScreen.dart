import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    loadTimer();
    super.initState();
  }

  Future<Timer> loadTimer() async {
    return Timer(Duration(seconds: 3), navigate);
  }

  navigate(){
    Navigator.of(context).pushReplacementNamed('/login');
  }





  @override
  Widget build(BuildContext context) {
    return Container(
    child: Image.asset("assets/1.png",fit:BoxFit.fill,),
    );
  }
}
