import 'package:baps_kms/Block/Style.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class observationDialog extends StatefulWidget {
  String name;
  String meaelid;
  observationDialog({this.name,this.meaelid});
  @override
  _observationDialogState createState() => _observationDialogState();
}

class _observationDialogState extends State<observationDialog> {
  var Selected ='Leftover';
  var _values = TextEditingController();
  void onchangestate(value){
    setState(() {
      Selected = value;
    });
  }
  bool remember = true;
  bool serVal = false;
  bool tuVal = false;
  bool wedVal = false;


  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 500,
          margin: EdgeInsets.symmetric(horizontal: 8.0,vertical: 8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: Center(child: Text(widget.name,style: dailogText,))),
                  InkWell(
                    child: Icon(Icons.highlight_off,color: Colors.black,),
                    onTap: (){
                      Navigator.pop(context);
                    },
                  )
                ],
              ),
              Divider(
                thickness: 0.3,
                color: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(3,0,0,0),
                child: Text('Observation',style: dailogText,),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 28.0,
                        width: 28.0,
                        child: Radio(
                          value: 'Leftover',
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          groupValue: Selected,
                          activeColor: Colors.orange,
                          onChanged: onchangestate,
                        ),
                      ),
                      Text('Leftover/Wastage',style: dailogText2)
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 28.0,
                        width: 28.0,
                        child: Radio(
                          value: 'Shortage',
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          groupValue: Selected,
                          activeColor: Colors.orange,
                          onChanged: onchangestate,
                        ),
                      ),
                      Text('Shortage',style: dailogText2,),
                    ],
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(3,0,0,0),
                child: Text('Note',style: dailogText,),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(3,0,3,0),
                child: TextField(
                  controller: _values,
                  expands: false,
                  maxLines: 7,
                  style: dailogText3,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.grey,
                        ),
                      ),
                      contentPadding: EdgeInsets.symmetric(vertical: 2.0,
                          horizontal: 10.0)
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                  SizedBox(
                    height: 24.0,
                    width: 24.0,
                    child: Checkbox(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      activeColor: primaryColor,
                      value: serVal,
                      onChanged: (bool value) {
                        setState(() {
                          serVal = value;
                        });
                      },
                    ),
                  ),
                  Text('Serving utensils',style: dailogText2,)
                ],
              ),
              // [Tuesday] checkbox
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 24.0,
                    width: 24.0,
                    child: Checkbox(
                     materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      activeColor: primaryColor,
                      value: tuVal,
                      onChanged: (bool value) {
                        setState(() {
                          tuVal = value;
                        });
                      },
                    ),
                  ),
                  Text('Serving equipment',style: dailogText2,),
                ],
              ),
              // [Wednesday] checkbox
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 24.0,
                    width: 24.0,
                    child: Checkbox(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      activeColor: primaryColor,
                      value: wedVal,
                      onChanged: (bool value) {
                        setState(() {
                          wedVal = value;
                        });
                      },
                    ),
                  ),
                  Text("Temperature",style: dailogText2,),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(3,0,3,0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: RaisedButton(
                        elevation: 0.0,
                        color: primaryColor,
                        child: Text('Update',style: TextStyle(
                          color: Colors.white
                        ),),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)
                        ),
                        onPressed: ()async{
                          FormData formdata = FormData.fromMap({
                            "menu_id": "${widget.meaelid}",
                            "obs": "$Selected",
                            "note": "${_values.text}",
                            "serving_utensils":"$serVal"
                          });
                          Dio dio = Dio();
                          Response response = await dio.post("http://134.209.159.163/baps/api/observation/obsadd",
                            data: formdata,
                          );
                          print(response);
                          Navigator.pop(context);

                        },
                      ),
                    ),
                  ],
                ),
              )


            ],
          ),
        ),
      ),
    );
  }
}
