import 'package:baps_kms/screen/Meal_data.dart';
import 'package:flutter/material.dart';

class MealPage extends StatefulWidget {
  @override
  _MealPageState createState() => _MealPageState();
}

class _MealPageState extends State<MealPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          tooltip: 'Back',
          icon: Icon(Icons.arrow_back,color:Colors.white,),
        ),
        title: Center(child: Text('Meal')),
        actions: <Widget>[
          Icon(Icons.search,color: Colors.white,
          ),
          SizedBox(
            width: 10.0,
          )
        ],
      ),
        body: MealEvent(),

    );
  }
}
