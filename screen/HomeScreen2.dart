import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class HomeScreen2 extends StatefulWidget {
  @override
  _HomeScreen2State createState() => _HomeScreen2State();
}

class _HomeScreen2State extends State<HomeScreen2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Image.asset("assets/4.jpg",
          ),
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.transparent,
              height: 400.0,
              width: 400.0,
              child: GridView.count(
                physics: ScrollPhysics(),
                padding: EdgeInsets.only(left: 20.0,right: 20.0,top: 40.0,bottom: 40.0),
                mainAxisSpacing: 15.0,
                crossAxisSpacing: 15.0,
                crossAxisCount: 2,
//                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  InkWell(
                    onTap: (){
                      Navigator.of(context).pushNamed('/admin');
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      child: Container(
                        width: MediaQuery.of(context).size.width/2.5,
                        height: MediaQuery.of(context).size.width/2.5,
                        child: SvgPicture.asset('assets/admin.svg'),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.of(context).pushNamed('/karyakarta');
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      child: Container(
                        child: SvgPicture.asset('assets/karyakarta.svg'),
                        width: MediaQuery.of(context).size.width/2.5,
                        height: MediaQuery.of(context).size.width/2.5,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.of(context).pushNamed('/meal');
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      child: Container(
                        child: SvgPicture.asset('assets/meal.svg',),
                      ),
                    ),
                  ),
                  ClipRRect(
                    borderRadius:BorderRadius.all(Radius.circular(20.0)) ,
                    child: Container(
                      child: SvgPicture.asset('assets/settings.svg'),
                      width: MediaQuery.of(context).size.width/2.5,
                      height: MediaQuery.of(context).size.width/2.5,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
