import 'package:baps_kms/Block/Style.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
class dialogbox extends StatefulWidget {
  int actualcount;
  String meal_id;
  dialogbox({this.actualcount,this.meal_id});

  @override
  _dialogboxState createState() => _dialogboxState();
}

class _dialogboxState extends State<dialogbox> {

  List _dinningArea = [
    "Male",
    "Female",
    "Male&Female",
    "Guest Dinning",
    "BKG Dinning",
    "Other"
  ];
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentArea = "";
  String buffet_station = "0";

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentArea = _dropDownMenuItems[0].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String areas in _dinningArea) {
      items.add(new DropdownMenuItem(value: areas, child: new Text(areas)));
    }
    return items;
  }
  var Selected ='add';

  var _values = TextEditingController();
  void changedDropDownItem(String selectedZone) {
    setState(() {
      _currentArea = selectedZone;
    });
  }
  var _buffText = TextEditingController();

  void onchangestate(value){
    setState(() {
      Selected = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 5.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: Container(
          margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
          height: 500, width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Actual Count till Now: '+ widget.actualcount.toString(),
                style: dailogText,
              ),
              Divider(
                color: Colors.grey,
                thickness: 0.2,
              ),
              Text('Dinning Area',
                  style: dailogText2),

              ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: Container(
                  padding: EdgeInsets.only(left: 5.0),
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    shape: BoxShape.rectangle
                  ),
                  child: DropdownButton(
                    style: dailogText3,
                    isDense: false,
                    isExpanded: true,
                    value: _currentArea,
                    items: _dropDownMenuItems,
                    onChanged: changedDropDownItem,
                    underline: Container(
                      decoration: BoxDecoration(
                        color: Colors.transparent
                      ),
                    ),
                  ),
                ),
              ),
              Text('Buffet Station', style: dailogText2),
              TextField(
                controller: _buffText,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1.0,
                        color: Colors.grey,
                      ),
                    ),
                    contentPadding: EdgeInsets.symmetric(vertical: 2.0,
                        horizontal: 10.0)
                ),
                keyboardType: TextInputType.numberWithOptions(),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 100.0,),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height:28.0,
                          width: 28.0,
                          child: Radio(
                            value: 'add',
                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            groupValue: Selected,
                            activeColor: Colors.orange,
                            onChanged: onchangestate,
                          ),
                        ),
                        Text('Add',style: dailogText3)
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height:28.0,
                          width: 28.0,
                          child: Radio(
                            value: 'Subtract',
                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            groupValue: Selected,
                            activeColor: Colors.orange,
                            onChanged: onchangestate,
                          ),
                        ),
                        Text('Subtract',style: dailogText3,),
                      ],
                    )
                  ],
                ),
              ),
              Text('Count',style: dailogText2,),
              TextField(
                controller: _values,
                expands: false,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1.0,
                        color: Colors.grey,
                      ),
                    ),
                    contentPadding: EdgeInsets.symmetric(vertical: 2.0,
                        horizontal: 10.0)
                ),
                keyboardType: TextInputType.numberWithOptions(),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    child: Text(
                      "Cancel",
                      style: TextStyle(color: Colors.white,
                      fontFamily: 'Sen'),
                    ),
                    color: primaryColor,
                    onPressed: () => Navigator.pop(context),
                  ),
                  RaisedButton(
                    color: primaryColor,
                    child: Text('Update',
                    style: TextStyle(color: Colors.white),),
                    shape: RoundedRectangleBorder(),
                    splashColor: Colors.orangeAccent,
                    onPressed: ()async{
                      FormData formdata = FormData.fromMap({
                        "meal_id": "${widget.meal_id}",
                        "dining_area": "$_currentArea",
                        "buffet_station": "${_buffText.text}",
                        "user_id": "1",
                        "operation": "$Selected",
                        "count":"${_values.text}",

                      });
                      Dio dio = Dio();
                      Response response = await dio.post("http://134.209.159.163/baps_kms/api/count/countadd",
                        data: formdata,
                      );
                      print(response);
                      Navigator.of(context).pushNamedAndRemoveUntil('/meal', ModalRoute.withName('/home'));

                    },

                  ),
                ],
              )
            ],
          )),
    );
  }
}
