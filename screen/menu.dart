import 'dart:convert';
import 'dart:core';

import 'package:baps_kms/Block/Style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:http/http.dart' as http;

import 'obserDialog.dart';

class MenuList extends StatefulWidget {
  List<String>names;
  String mealid;
  List is_avial;
  MenuList({this.names,this.mealid,this.is_avial});

  @override
  _MenuListState createState() => _MenuListState();
}

class _MenuListState extends State<MenuList> {
  List _dinningArea = [
    "Male",
    "Female",
    "Male&Female",
    "Guest Dinning",
    "BKG Dinning",
    "Other"
  ];
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentArea = "";
  String buffet_station = "0";

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentArea = _dropDownMenuItems[0].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String areas in _dinningArea) {
      items.add(new DropdownMenuItem(value: areas, child: new Text(areas)));
    }
    return items;
  }

  void changedDropDownItem(String selectedZone) {
    setState(() {
      _currentArea = selectedZone;
    });
  }

  List _selecteCategorys = List();
  bool remember = true;

  void _onCategorySelected(bool selected, category_id) {
    if (selected == true) {
      setState(() {
        _selecteCategorys.add(category_id);
      });
    } else {
      setState(() {
        _selecteCategorys.remove(category_id);
      });
    }
  }

  List<bool>remembers = [];


  @override
  Widget build(BuildContext context) {
    List ofarrayid;
    List replaced;
    Map<String,TextEditingController> controller;


    for (int i = 0; i < widget.names.length; i++) {
      remembers.add(true);}
//    Map<String,TextEditingController> textEditingControllers = {};
//    var textFields = <TextField>[];
//    widget.names.forEach((str){
//      var textEditingController = new TextEditingController(text:str);
//      textEditingControllers.putIfAbsent(str, ()=>textEditingController);
//      return textFields.add( TextField(controller: textEditingController,
//      ));
//    });


      print(widget.is_avial);
      return Scaffold(
        appBar: AppBar(
          title: Text('Menu'),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                child: ListView.builder(
                    itemCount: widget.names.length,
                    itemBuilder: (context,index){
                    return Container(
                      child: Row(
                        children: <Widget>[
                          Checkbox(
                            activeColor: primaryColor,
                            value: remembers[index],
                            onChanged: (value){
                              setState(() {
                                remembers[index]=value;
                              });
                            },
                          ),
                          SizedBox(
                              width: 110,
                              child: Text(widget.names[index])),
                          !remembers[index]?
                              SizedBox(
                                width: 110,
                                child: TextField(
                                ),
                              ):SizedBox(
                            width: 110,
                          )
                        ],
                      ),
                    );
                    }

                ),
              ),
            ),
            RaisedButton(
              color: primaryColor,
              onPressed: (){},
              child: Text('next'),
            )
          ],
        ),
      );
  }
}