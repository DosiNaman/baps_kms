import 'package:baps_kms/Block/LoginBLock2.dart';
import 'package:baps_kms/Block/LoginBLock3.dart';
import 'package:baps_kms/Block/LoginBlock1.dart';
import 'package:baps_kms/Block/Style.dart';
//import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Login2 extends StatefulWidget {


  @override
  _Login2State createState() => _Login2State();
}

class _Login2State extends State<Login2> {



  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors:  [
          Color.fromRGBO(229,142,46,1),
          Color.fromRGBO(207,147,68,1),
          ],
          begin:Alignment.centerLeft ,
          end: Alignment.centerRight,
        )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              "assets/4.jpg",colorBlendMode: BlendMode.darken,
            ),

            Expanded(
              flex: 1,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0)),
                child: Container(
                  padding: EdgeInsets.only(left: 40.0,top: 15.0,right: 40.0,bottom: 10.0),
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      LBlock1(),
                      LBlock2(),
                      LBlock3(),
                      MaterialButton(
                        onPressed: (){
                          Navigator.of(context).pushNamed('/home');
                        },
                        elevation: 2.0,
                        minWidth:MediaQuery.of(context).size.width,
                        child: Text('Login',
                          style: TextStyle(
                              fontSize: 28.0,
                              fontFamily: 'Sen',
                              color: Colors.white
                          ),),
                        color: Colors.orange[500],
                        padding: EdgeInsets.all(10.0),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            InkWell(
                              onTap: (){},
                              child: Text("Create New Account",
                                style: TextStyle(
                                    color: primaryColor,

                                ),),
                            ),
                            InkWell(
                              onTap: (){},
                              child: Text("Forget Password ?",
                                style: TextStyle(
                                    color: primaryColor,
                                ),),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
